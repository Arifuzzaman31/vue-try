
require('./bootstrap');

window.Vue = require('vue');

import VueRouter from 'vue-router'
Vue.use(VueRouter);


import {routes} from './routes'

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('admain', require('./components/admin/adminMaster.vue'));



const router = new VueRouter({
  routes: routes,
  mode: 'history'
});

const app = new Vue({
    el: '#app',
    router, 
});
